import { set } from '@/utils/vuex'

export const state = () => ({
  drawer: null
})

export const mutations = {
  setDrawer: set('drawer')
}

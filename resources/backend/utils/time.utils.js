import moment from 'moment'
export const ISO_FORMAT = 'YYYY-MM-DD'

export const TODAY = moment().format(ISO_FORMAT)
export const YESTERDAY = moment().subtract(1, 'day').format(ISO_FORMAT)
export const LAST_MONTH_START = moment().subtract(1, 'month').startOf('month').format(ISO_FORMAT)
export const LAST_MONTH_END = moment().subtract(1, 'month').endOf('month').format(ISO_FORMAT)
// format(new Date(new Date().getFullYear(), new Date().getMonth(), 0), ISO_FORMAT)
export const THIS_MONTH_START = moment().startOf('month').format(ISO_FORMAT)
export const THIS_MONTH_END = moment().endOf('month').format(ISO_FORMAT)
export const THIS_YEAR_START = moment().startOf('year').format(ISO_FORMAT)
export const THIS_YEAR_END = moment().endOf('year').format(ISO_FORMAT)
export const LAST_YEAR_START = moment().subtract(1, 'year').startOf('year').format(ISO_FORMAT)
export const LAST_YEAR_END = moment().subtract(1, 'year').endOf('year').format(ISO_FORMAT)
function getDaysBefore (days) {
  return moment().subtract(days, 'day').format(ISO_FORMAT)
}

export const getMaxEndOfMonth = function () {
  if (THIS_MONTH_END > TODAY) {
    return TODAY
  }
  return THIS_MONTH_END
}

export default {
  getDaysBefore
}

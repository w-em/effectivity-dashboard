const { join } = require('path')
const { copySync, removeSync } = require('fs-extra')
require('dotenv').config()
// const BASE_URL = 'https://effectivity.w-em.com'
// const BASE_URL = 'http://localhost'
const development = process.env.NODE_ENV !== 'production'
const BASE_URL = development ? 'http://localhost' : 'https://effectivity.w-em.com'

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  srcDir: 'resources/backend',

  publicRuntimeConfig: {
    baseUrl: process.env.BASE_URL || BASE_URL,
    apiUrl: process.env.API_URL || BASE_URL,
    axios: {
      browserBaseURL: process.env.API_URL || BASE_URL
    }
  },
  privateRuntimeConfig: {
    apiSecret: process.env.API_SECRET,
    axios: {
      baseURL: process.env.BASE_URL || BASE_URL
    }
  },
  env: {
    baseUrl: process.env.BASE_URL || BASE_URL,
    apiUrl: process.env.API_URL || BASE_URL,
    appName: process.env.APP_NAME || 'WEM-Dashboard',
    appLocale: process.env.APP_LOCALE || 'en'
  },

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - WEM-Dashboard',
    title: 'WEM-Dashboard',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: { color: '#3dcd58' },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/fonts',
    '~plugins/i18n',
    '~plugins/vform',
    '~plugins/apexchart',
    '~plugins/axios',
    '~plugins/tipTapVuetify',
    '~plugins/api/countryApiService',
    '~plugins/api/teamApiService',
    '~plugins/api/personalApiService',
    '~plugins/api/companyApiService',
    '~plugins/api/personalReportApiService',
    '~plugins/api/projectApiService'
  ],

  router: {
    middleware: ['auth', 'locale']
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    ['@nuxtjs/dotenv', { filename: '.env' }],
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/auth-next'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.API_URL,
    proxy: true,
    credentials: true
  },

  proxy: {
    '/api': {
      target: BASE_URL,
      pathRewrite: { '^/api': '/api' }
    }
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#1FA22E',
          secondary: '#39A3F5',
          background: '#E9E9E9'
        },
        dark: {
          primary: '#1FA22E',
          secondary: '#39A3F5',
          background: '#E9E9E9'
        }
      }
    },
    defaultAssets: false
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
    publicPath: '/_backend/'
  },

  auth: {
    watchLoggedIn: true,
    rewriteRedirects: true,
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/login',
      home: '/'
    },
    strategies: {
      local: {
        tokenRequired: true,
        resetOnError: true,
        rewriteRedirects: false,
        token: {
          property: 'data.token',
          global: true,
          required: true,
          maxAge: 84600,
          // type: 'Bearer'
          type: false
        },
        user: {
          property: 'user',
          autoFetch: true
        },
        endpoints: {
          login: { url: '/api/auth/login', method: 'post' },
          logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/api/auth/me', method: 'get' }
        }
      },
      laravelJWT: {
        provider: 'laravel/jwt',
        url: BASE_URL,
        endpoints: {
          login: { url: '/api/auth/login', method: 'post' },
          logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/api/auth/me', method: 'get' }
        },
        token: {
          property: 'access_token',
          maxAge: 60 * 59,
          type: 'Bearer'
        },
        refreshToken: {
          maxAge: 20160 * 60,
          property: 'refresh_token',
          data: 'refresh_token'
        }
      }
    }
  },

  hooks: {
    build: {
      done (generator) {
        // Copy dist files to public/_nuxt
        if (generator.nuxt.options.dev === false && generator.nuxt.options.ssr === false) {
          const publicDir = join(generator.nuxt.options.rootDir, 'public', '_backend')
          const viewsDir = join(generator.nuxt.options.rootDir, 'resources', 'views')
          removeSync(publicDir)
          copySync(join(generator.nuxt.options.rootDir, '.nuxt', 'dist', 'client'), publicDir)
          copySync(join(generator.nuxt.options.rootDir, '.nuxt', 'dist', 'server', 'index.spa.html'), join(viewsDir, 'backend.blade.php'))
          removeSync(generator.nuxt.options.generate.dir)
        }
      }
    }
  }
}

import notificationMixin from '~/mixins/notification.mixin'
export default {
  mixins: [notificationMixin],
  props: {
    id: {
      type: Number,
      required: false
    },
    edit: {
      type: Boolean,
      required: false
    },
    create: {
      type: Boolean,
      required: false
    }
  },
  data: () => ({
    error404: false,
    form: {},
    _errorFields: [],
    errors: {},
    loading: {}
  }),
  computed: {
    isLoading () {
      let loading = false
      Object.values(this.loading).forEach((loadState) => {
        if (loadState) {
          loading = true
        }
      })
      return loading
    }
  },
  methods: {
    clearErrors (key = null) {
      const me = this
      if (!key) {
        for (const key in me.errors) {
          me.errors[key]['error-messages'] = ''
          me.errors[key].error = false
        }
        me.$forceUpdate()
      } else {
        try {
          me.errors[key]['error-messages'] = ''
          me.errors[key].error = false
        } catch (error) {}
      }
      me.$forceUpdate()
    },
    load () {
      return new Promise((resolve, reject) => {
        this.getApiService().getById(this.id)
          .then((response) => {
            this.form = response
            this.error404 = false
            resolve(response)
          })
          .catch((error) => {
            this.error404 = true
            reject(error)
          })
      })
    },
    store (callback) {
      const me = this
      const valide = me.$refs.form.validate()

      return new Promise((resolve, reject) => {
        if (valide) {
          me.getApiService().create(me.form)
            .then((response) => {
              me.createNotificationSuccess({
                title: 'Erfolgreich',
                message: 'Daten erfolgreich gespeichert!'
              })
              resolve(response)
            })
            .catch((error) => {
              let msg = error.response.data.message

              if (error.response.data.errors) {
                for (const key in error.response.data.errors) {
                  msg += '<br />' + error.response.data.errors[key]
                  try {
                    me.errors[key]['error-messages'] = error.response.data.errors[key]
                    me.errors[key].error = true
                  } catch (error) {}
                }
                me.$forceUpdate()
              }

              me.createNotificationError({
                title: 'Fehler',
                message: msg
              })
            })
        } else {
          // reject('invalid form')
        }
      })
    },
    update () {
      const me = this

      return new Promise((resolve, reject) => {
        const valide = me.$refs.form.validate()
        if (valide) {
          me.getApiService().updateById(me.id, me.form)
            .then((response) => {
              /*
              me.createNotificationSuccess({
                title: 'Erfolgreich',
                message: 'Daten erfolgreich gespeichert!'
              })

               */
              resolve(response)
            })
            .catch((error) => {
              const msgs = []
              if (error.response.data.errors) {
                for (const key in error.response.data.errors) {
                  try {
                    me.errors[key]['error-messages'] = error.response.data.errors[key]
                    me.errors[key].error = true

                    msgs.push(error.response.data.errors[key])
                  } catch (e) {}
                }
                me.$forceUpdate()
              } else {
                msgs.push(error.response.data.message)
              }

              me.createNotificationError({
                title: 'Fehler',
                message: msgs.join('<br />')
              })
              reject(error)
            })
        } else {
          reject(valide)
        }
      })
    },
    getApiService () {
      throw new Error('getApiService() function have to be defined')
    },
    generateErrorFields (fields = []) {
      fields.forEach((item) => {
        this.errors[item] = {
          error: false,
          'error-messages': []
        }
      })
    }
  },
  created () {
    this.generateErrorFields(this.errorFields)
  }
}

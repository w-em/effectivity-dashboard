import types from '@/utils/types.utils'
import debug from '@/utils/debug.utils'
const debounce = require('debounce')

const defaultPagePage = 10

export default {
  data () {
    return {
      page: 1,
      items: [],
      itemsPerPageOptions: [10, 25, 50, 100],
      itemsPerPage: defaultPagePage,
      total: 0,
      sortBy: null,
      sortDirection: 'ASC',
      selection: [],
      loading: {
        items: false
      },
      term: undefined,
      disableRouteParams: false,
      options: {
        groupDesc: [],
        groupBy: [],
        itemsPerPage: defaultPagePage,
        multiSort: false,
        mustSort: false,
        page: 1,
        sortBy: [],
        sortDesc: [false]
      }
    }
  },

  computed: {
    sortDescending: {
      get () {
        return this.sortDirection.toUpperCase() === 'DESC'
      },
      set (newVal) {
        if (newVal) {
          this.sortDirection = 'DESC'
        } else {
          this.sortDirection = 'ASC'
        }
      }

    },
    maxPage () {
      return Math.ceil(this.total / this.itemsPerPage)
    },

    routeName () {
      return this.$route.name
    },

    isLoading () {
      let loading = false
      Object.values(this.loading).forEach((loadState) => {
        if (loadState) {
          loading = true
        }
      })
      return loading
    }
  },

  created () {
  },

  mounted () {
    const self = this
    if (self.disableRouteParams) {
      self.getList()
      return
    }
    const actualQueryParameters = self.$route.query

    // When no route information are provided
    if (types.isEmpty(actualQueryParameters)) {
      self.resetListing()
    } else {
      // otherwise update local data and fetch from server
      self.updateData(actualQueryParameters)
      self.getList()
    }
  },

  watch: {
    // Watch for changes in query parameters and update listing
    '$route' () {
      if (this.disableRouteParams) {
        return
      }

      const query = this.$route.query

      if (types.isEmpty(query)) {
        this.resetListing()
      }

      // Update data information from the url
      this.updateData(query)

      // Fetch new list
      this.getList()
    },
    options: {
      handler (newVal) {
        const opts = {}

        if (newVal.itemsPerPage !== this.itemsPerPage) {
          opts.itemsPerPage = newVal.itemsPerPage
        }

        if (newVal.page !== this.page) {
          opts.page = newVal.page
        }

        if (newVal.sortBy[0] !== this.sortBy) {
          opts.sortBy = newVal.sortBy[0]
        }

        const sortDesc = newVal.sortDesc[0] === true ? 'DESC' : 'ASC'
        if (sortDesc !== this.sortDirection) {
          opts.sortDirection = sortDesc
        }

        if (Object.keys(opts).length > 0) {
          this.updateRoute(opts)
        }
      },
      deep: true
    },
    term: debounce(function (newVal) {
      this.onSearch(newVal)
    }, 300)
  },

  methods: {
    updateData (customData) {
      this.page = parseInt(customData.page, 10) || this.page
      this.itemsPerPage = parseInt(customData.itemsPerPage, 10) || this.itemsPerPage
      this.term = customData.term || this.term
      this.sortBy = customData.sortBy || this.sortBy
      this.sortDirection = customData.sortDirection || this.sortDirection

      this.options.page = this.page
      this.options.itemsPerPage = this.itemsPerPage
      this.options.sortBy = [this.sortBy]
      this.options.sortDesc = [this.sortDirection === 'DESC']
    },
    updateRoute (customQuery, queryExtension = {}) {
      const self = this
      // Get actual query parameter
      const query = customQuery || this.$route.query
      const routeQuery = this.$route.query
      const newQuery = {}
      const itemsPerPage = query.itemsPerPage || this.itemsPerPage
      if (itemsPerPage !== defaultPagePage) {
        newQuery.itemsPerPage = itemsPerPage
      }
      const page = query.page || this.page

      if (page !== 1) {
        newQuery.page = page
      }
      const sortBy = query.sortBy || this.sortBy
      if (sortBy) {
        newQuery.sortBy = sortBy
      }
      const sortDirection = query.sortDirection || this.sortDirection
      if (sortDirection !== 'ASC') {
        newQuery.sortDirection = sortDirection
      }
      const route = {
        name: this.$route.name,
        params: this.$route.params,
        query: {
          term: query.term || this.term,
          ...newQuery,
          ...queryExtension
        }
      }

      // If query is empty then replace route, otherwise push
      if (types.isEmpty(routeQuery)) {
        self.$router.replace(route)
      } else {
        self.$router.push(route)
      }
    },

    resetListing () {
      this.updateRoute({
        name: this.$route.name,
        query: {
          itemsPerPage: this.itemsPerPage,
          page: this.page,
          term: this.term,
          sortBy: this.sortBy,
          sortDirection: this.sortDirection
        }
      })
    },

    getListingParams () {
      if (this.disableRouteParams) {
        return {
          itemsPerPage: this.itemsPerPage,
          page: this.page,
          term: this.term,
          sortBy: this.sortBy !== null ? [this.sortBy] : null,
          sortDesc: [this.sortDirection]
        }
      }
      // Get actual query parameter
      const query = this.$route.query
      const sortBy = query.sortBy || this.sortBy || ''
      const params = {
        itemsPerPage: query.itemsPerPage || defaultPagePage,
        page: query.page || 1,
        term: query.term || '',
        sortDesc: [query.sortDirection === 'DESC'] || [this.sortDirection === 'DESC'],
        sortBy: [sortBy]
      }

      return params
    },

    onPageChange (opts) {
      this.page = opts.page
      this.itemsPerPage = opts.itemsPerPage
      if (this.disableRouteParams) {
        this.getList()
        return
      }
      this.updateRoute({
        page: this.page
      })
    },

    onSearch (value) {
      this.term = value

      if (this.disableRouteParams) {
        this.page = 1
        this.getList()
        return
      }

      this.updateRoute({
        term: this.term,
        page: 1
      })
    },

    onSortColumn (column) {
      if (this.disableRouteParams) {
        if (this.sortBy === column.dataIndex) {
          this.sortDirection = (this.sortDirection === 'ASC' ? 'DESC' : 'ASC')
        } else {
          this.sortDirection = 'ASC'
          this.sortBy = column.dataIndex
        }
        this.getList()
        return
      }

      if (this.sortBy === column.dataIndex) {
        this.updateRoute({
          sortDirection: (this.sortDirection === 'ASC' ? 'DESC' : 'ASC')
        })
      } else {
        this.updateRoute({
          sortBy: column.dataIndex,
          sortDirection: 'ASC'
        })
      }
      this.updateRoute()
    },

    onRefresh () {
      this.getList()
    },

    getList () {
      debug.warn(
        'Listing Mixin',
        'When using the listing mixin you have to implement your custom "getList()" method.'
      )
    }
  }
}

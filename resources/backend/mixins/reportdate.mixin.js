import moment from 'moment'
import types from '@/utils/types.utils'
import debug from '@/utils/debug.utils'

export default {
  data () {
    return {
      disableDateRouteParams: false,
      reportdate: {
        from: moment().startOf('year').format('YYYY-MM-DD'),
        to: moment().format('YYYY-MM-DD')
      }
    }
  },
  mounted () {
    const self = this
    if (self.disableDateRouteParams) {
      self.fetchReportData()
      return
    }

    const actualQueryParameters = self.$route.query
    // When no route information are provided
    if (types.isEmpty(actualQueryParameters)) {
      self.resetParams()
    } else {
      // otherwise update local data and fetch from server
      self.updateData(actualQueryParameters)
      self.fetchReportData()
    }
  },
  watch: {
    // Watch for changes in query parameters and update listing
    '$route' () {
      if (this.disableDateRouteParams) {
        return
      }

      const query = this.$route.query

      if (types.isEmpty(query)) {
        this.resetParams()
      }

      // Update data information from the url
      this.updateData(query)

      // Fetch new list
      this.fetchReportData()
    }
  },
  computed: {
    datePickerDates () {
      return {
        start: this.reportdate.from,
        end: this.reportdate.to
      }
    }
  },
  methods: {
    onDateChange (val) {
      this.updateRoute({
        from: val.start,
        to: val.end
      })
    },
    updateData (customData) {
      this.reportdate.from = moment(customData.from).format('YYYY-MM-DD') || this.reportdate.from
      this.reportdate.to = moment(customData.to).format('YYYY-MM-DD') || this.reportdate.to
    },
    updateRoute (customQuery, queryExtension = {}) {
      const self = this
      // Get actual query parameter
      const query = customQuery || this.$route.query
      const routeQuery = this.$route.query

      const newQuery = {
        from: query.from || this.reportdate.from,
        to: query.to || this.reportdate.to
      }
      const route = {
        name: this.$route.name,
        params: this.$route.params,
        query: {
          ...newQuery,
          ...queryExtension
        }
      }

      // If query is empty then replace route, otherwise push
      if (types.isEmpty(routeQuery)) {
        self.$router.replace(route)
      } else {
        self.$router.push(route)
      }
    },
    resetParams () {
      this.updateRoute({
        name: this.$route.name,
        query: {
          from: this.reportdate.from,
          to: this.reportdate.to
        }
      })
    },
    fetchReportData () {
      debug.warn(
        'Reportdate Mixin',
        'When using the reportdate mixin you have to implement your custom "fetchReportData()" method.'
      )
    }
  }
}

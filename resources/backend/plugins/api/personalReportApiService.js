import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class Service extends ApiService {
    constructor (apiEndpoint = 'api/report/personals') {
      super($axios, apiEndpoint)
      this.name = 'personalReportApiService'
    }

    /**
     * Gets a list from the configured API end point using the page & limit.
     *
     * @param {Number} page
     * @param {Number} itemsPerPage
     * @param {Array} sortBy
     * @param {Array} sortDesc
     * @param {String} term
     * @param {Number} year
     * @returns {Promise<T>}
     */
    getAvailablityList ({
      page = 1,
      itemsPerPage = 50,
      sortBy,
      sortDesc,
      term,
      year
    }) {
      const requestHeaders = this.getBasicHeaders({})
      const params = { page, itemsPerPage, sortBy, sortDesc, year }

      if (term) {
        params.term = term
      }

      return this.httpClient
        .get(this.getApiBasePath('availabilities'), { params, headers: requestHeaders })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }
  }

  inject('personalReportApiService', new Service())
}

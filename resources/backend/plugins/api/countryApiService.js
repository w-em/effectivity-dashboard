import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class Service extends ApiService {
    constructor (apiEndpoint = 'api/countries') {
      super($axios, apiEndpoint)
      this.name = 'countryApiService'
    }
  }

  inject('countryApiService', new Service())
}

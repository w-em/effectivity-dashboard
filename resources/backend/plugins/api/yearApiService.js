import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class YearApiService extends ApiService {
    constructor (apiEndpoint = 'api/years') {
      super($axios, apiEndpoint)
      this.name = 'yearApiService'
    }
  }

  inject('yearApiService', new YearApiService())
}

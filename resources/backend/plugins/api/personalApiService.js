import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class Service extends ApiService {
    constructor (apiEndpoint = 'api/personals') {
      super($axios, apiEndpoint)
      this.name = 'personalApiService'
    }

    /**
     * Gets a list from the configured API end point using the page & limit.
     *
     * @param {Number} page
     * @param {Number} itemsPerPage
     * @param {Array} sortBy
     * @param {Array} sortDesc
     * @param {String} term
     * @returns {Promise<T>}
     */
    getReportList ({
      page = 1,
      itemsPerPage = 50,
      sortBy,
      sortDesc,
      term
    }) {
      const requestHeaders = this.getBasicHeaders({})
      const params = { page, itemsPerPage, sortBy, sortDesc }

      if (term) {
        params.term = term
      }

      return this.httpClient
        .get(this.getApiBasePath('reports'), { params, headers: requestHeaders })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    /**
     * Get the detail entity from the API end point using the provided entity id.
     *
     * @param {String|Number} id
     * @param {Object} additionalParams
     * @param {Object} additionalHeaders
     * @returns {Promise<T>}
     */
    getReportOverviewById (id, additionalParams = {}, additionalHeaders = {}) {
      if (!id) {
        return Promise.reject(new Error('Missing required argument: id'))
      }

      const params = additionalParams
      const headers = this.getBasicHeaders(additionalHeaders)

      return this.httpClient
        .get(this.getApiBasePath(id + '/reportOverview'), {
          params,
          headers
        })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    getProjects (id, additionalParams = {}, additionalHeaders = {}) {
      if (!id) {
        return Promise.reject(new Error('Missing required argument: id'))
      }

      const params = additionalParams
      const headers = this.getBasicHeaders(additionalHeaders)

      return this.httpClient
        .get(this.getApiBasePath(id + '/projects'), {
          params,
          headers
        })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    getProjectDetails (id, additionalParams = {}, additionalHeaders = {}) {
      if (!id) {
        return Promise.reject(new Error('Missing required argument: id'))
      }

      const params = additionalParams
      const headers = this.getBasicHeaders(additionalHeaders)

      return this.httpClient
        .get(this.getApiBasePath(id + '/projectDetails'), {
          params,
          headers
        })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }

    /**
     * Get the detail entity from the API end point using the provided entity id.
     *
     * @param {String|Number} id
     * @param {Number} year
     * @param {Object} additionalParams
     * @param {Object} additionalHeaders
     * @returns {Promise<T>}
     */
    getHolidayByIdAndYear (id, year, additionalParams = {}, additionalHeaders = {}) {
      if (!id) {
        return Promise.reject(new Error('Missing required argument: id'))
      }
      if (!year) {
        return Promise.reject(new Error('Missing required argument: year'))
      }

      const params = additionalParams
      const headers = this.getBasicHeaders(additionalHeaders)

      return this.httpClient
        .get(this.getApiBasePath(id + '/holidays/' + year), {
          params,
          headers
        })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }
  }

  inject('personalApiService', new Service())
}

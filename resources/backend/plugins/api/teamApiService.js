import ApiService from './api.service'

export default ({ $axios }, inject) => {
  class Service extends ApiService {
    constructor (apiEndpoint = 'api/teams') {
      super($axios, apiEndpoint)
      this.name = 'teamApiService'
    }

    /**
     * Get the detail entity from the API end point using the provided entity id.
     *
     * @param {String|Number} id
     * @param {Object} additionalParams
     * @param {Object} additionalHeaders
     * @returns {Promise<T>}
     */
    getReportOverviewById (id, additionalParams = {}, additionalHeaders = {}) {
      if (!id) {
        return Promise.reject(new Error('Missing required argument: id'))
      }

      const params = additionalParams
      const headers = this.getBasicHeaders(additionalHeaders)

      return this.httpClient
        .get(this.getApiBasePath(id + '/reportOverview'), {
          params,
          headers
        })
        .then((response) => {
          return ApiService.handleResponse(response)
        })
    }
  }

  inject('teamApiService', new Service())
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('team_personals', function (Blueprint $table) {
            $table->unsignedBigInteger('team_id')->index();
            $table->unsignedBigInteger('personal_id')->index();
            $table->boolean('is_default')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
        Schema::dropIfExists('team_personals');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('country_day_offs', function (Blueprint $table) {
            $table->unsignedBigInteger('country_id')->index();
            $table->unsignedBigInteger('day_off_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('country_day_offs');
    }
}

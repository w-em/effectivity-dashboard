<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_records', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->unsignedBigInteger('personal_id')->index();
            $table->unsignedBigInteger('task_id')->index();
            $table->unsignedBigInteger('record_type_id')->index();
            $table->unsignedBigInteger('project_id')->index();
            $table->boolean('is_billable')->default(false);
            $table->float('value',8, 2, true);
            $table->date('record_date')->index();

            $table->index(['task_id', 'is_billable']);
            $table->index(['task_id', 'personal_id']);
            $table->index(['task_id', 'project_id']);
            $table->index(['task_id', 'record_type_id']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_records');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('availabilities', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->unsignedBigInteger('availability_type_id');
            $table->unsignedBigInteger('personal_id')->index();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('availability_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('availabilities');
        Schema::dropIfExists('availability_types');
    }
}

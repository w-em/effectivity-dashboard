# prjects
SELECT id, name as title, category_id as category_id, label_id as type_id, completed_on as completed_at, created_on as created_at, updated_on as updated_at, trashed_on as deleted_at FROM `projects`
- replace
   (`id`, `name`, `category_id`, `label_id`, `completed_on`, `created_on`, `updated_on`, `trashed_on`)
   (`id`, `title`, `category_id`, `type_id`, `completed_at`,  `created_at`, `updated_at`, `deleted_at`)

# tasks
SELECT id, summary, user_id as member_id, parent_id as task_id, value, created_on as created_at, updated_on as updated_at, trashed_on as deleted_at
 FROM `time_records` WHERE parent_type = 'TASK' ORDER BY `id` DESC
 - replace
    (`id`, `summary`, `user_id`, `parent_id`, `value`, `created_on`, `updated_on`, `trashed_on`) with
    (`id`, `summary`, `member_id`, `task_id`, `value`, `created_at`, `updated_at`, `deleted_at`)

#members
SELECT CONCAT(first_name, ' ', last_name) as name, email, 40, is_archived FROM `users` where type != 'Client'
- replace
    `users` (`first_name`, ` `, `last_name`, `email`)
    `members` (`name`, `email`, `weekly_work_hours`, `archive`)

#categories = project_categories
SELECT id, name, created_on, updated_on FROM `categories`
- replace
    INSERT INTO `categories` (`id`, `name`, `created_on`, `updated_on`)
    INSERT INTO `project_categories` (`id`, `title`, `created_at`, `updated_at`)

labels = project_types
SELECT id, name, updated_on as updated_at FROM `labels` where type = 'ProjectLabel'
- replace
    `labels` (`id`, `name`, `updated_on`)
    `project_types` (`id`, `title`, `updated_at`)

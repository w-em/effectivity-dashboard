<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\DayOff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert([
            'name' => 'Germany'
        ]);
        Country::insert([
            'name' => 'Serbia'
        ]);
    }
}

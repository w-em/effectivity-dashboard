<?php

namespace Database\Seeders;

use App\Models\ProjectCategory;
use App\Models\TimeRecord;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeRecordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, summary as name, user_id as personal_id, parent_id as task_id, job_type_id as record_type_id, record_date, billable_status as is_billable, value, created_on as created_at, updated_on as updated_at, trashed_on as deleted_at
 FROM `time_records` WHERE parent_type = 'TASK' and user_id is not null group by id";
        $results = DB::connection('mysql_active_collab')->select($sql, []);

        foreach (array_chunk($results, 5000) as $chunk) {
            TimeRecord::insert(json_decode(json_encode($chunk), true));
        }


    }
}

<?php

namespace Database\Seeders;

use App\Models\ProjectCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, created_on as created_at, updated_on as updated_at FROM `categories`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);
        ProjectCategory::insert(json_decode(json_encode($results), true));
        /*
        $file = __DIR__ . '/project_categories.sql';
        \Illuminate\Support\Facades\DB::unprepared(file_get_contents($file));*/
    }
}

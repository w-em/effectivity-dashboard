<?php

namespace Database\Seeders;

use App\Models\ProjectType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, updated_on as updated_at FROM `labels` where type = 'ProjectLabel'";
        $results = DB::connection('mysql_active_collab')->select($sql, []);
        ProjectType::insert(json_decode(json_encode($results), true));
    }
}

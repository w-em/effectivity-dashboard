<?php

namespace Database\Seeders;

use App\Models\Availability;
use App\Models\AvailabilityType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AvailabilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT  id, message as name, start_date, end_date, availability_type_id, created_on as created_at, updated_on as updated_at, user_id as personal_id FROM `availability_records`";
        $records = DB::connection('mysql_active_collab')->select($sql, []);
        Availability::insert(json_decode(json_encode($records), true));

        $sql = "SELECT  id, name, created_on as created_at, updated_on as updated_at FROM `availability_types`";
        $records = DB::connection('mysql_active_collab')->select($sql, []);
        AvailabilityType::insert(json_decode(json_encode($records), true));

        // $file = __DIR__ . '/projects.sql';
        // \Illuminate\Support\Facades\DB::unprepared(file_get_contents($file));
    }
}

<?php

namespace Database\Seeders;

use App\Models\Personal;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, CONCAT(first_name, ' ', last_name) as name, first_name, last_name, email, 40 as weekly_work_hours, 30 as vacation, archived_on as archived_at, created_on as created_at, updated_on as updated_at FROM `users` where type != 'Client'";
        $results = DB::connection('mysql_active_collab')->select($sql, []);

        $results = json_decode(json_encode($results), true);
        foreach ($results as $result) {

            if (is_null($result['name'])) {
                $result['name'] = $result['email'];
            }
            if ($result['id'] === 149) {
                $result['weekly_work_hours'] = 20;
            }

            Personal::insert($result);
        }

    }
}

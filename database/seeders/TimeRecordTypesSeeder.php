<?php

namespace Database\Seeders;

use App\Models\ProjectCategory;
use App\Models\TimeRecord;
use App\Models\TimeRecordType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TimeRecordTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, updated_on as updated_at
 FROM `job_types`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);

        foreach (array_chunk($results, 5000) as $chunk) {
            TimeRecordType::insert(json_decode(json_encode($chunk), true));
        }


    }
}

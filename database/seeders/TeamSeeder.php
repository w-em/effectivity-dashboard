<?php

namespace Database\Seeders;

use App\Models\Personal;
use App\Models\Team;
use App\Models\TimeRecord;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, created_on as created_at, updated_on as updated_at FROM `teams`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);
        Team::insert(json_decode(json_encode($results), true));

        $sql = "SELECT team_id, user_id as personal_id FROM `team_users`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);

        $datas = json_decode(json_encode($results), true);

        $personalIds = [];

        foreach ($datas as $data) {
            if (!in_array($data['personal_id'], $personalIds)) {
                DB::insert('insert into team_personals (team_id, personal_id, is_default) values (?, ?, 1)', array_values($data));
                $personalIds[] = $data['personal_id'];
            } else {
                DB::insert('insert into team_personals (team_id, personal_id, is_default) values (?, ?, 0)', array_values($data));
            }

        }

        // DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
    }
}

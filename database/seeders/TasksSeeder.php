<?php

namespace Database\Seeders;

use App\Models\ProjectCategory;
use App\Models\Task;
use App\Models\TimeRecord;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, project_id, name, estimate, is_billable, created_on as created_at, updated_on as updated_at FROM `tasks`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);

        foreach (array_chunk($results, 5000) as $chunk) {
            Task::insert(json_decode(json_encode($chunk), true));
        }


    }
}

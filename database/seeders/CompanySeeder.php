<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, address, homepage_url, phone, archived_on as archived_at, created_on as created_at, updated_on as updated_at FROM `companies`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);
        Company::insert(json_decode(json_encode($results), true));

    }
}

<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, category_id as category_id, company_id, label_id as project_type_id, completed_on as completed_at, created_on as created_at, updated_on as updated_at, trashed_on as deleted_at FROM `projects`";
        $projects = DB::connection('mysql_active_collab')->select($sql, []);

        $projects = json_decode(json_encode($projects), true);
        foreach ($projects as $project) {
            if ($project['project_type_id'] === 71) {
                $project['estimate'] = 4;
            }
            if ($project['project_type_id'] === 83) {
                $project['estimate'] = 280;
            }

            Project::insert($project);
        }


        // $file = __DIR__ . '/projects.sql';
        // \Illuminate\Support\Facades\DB::unprepared(file_get_contents($file));
    }
}

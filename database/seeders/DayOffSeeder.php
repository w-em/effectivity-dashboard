<?php

namespace Database\Seeders;

use App\Models\DayOff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DayOffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "SELECT id, name, start_date, end_date, repeat_yearly, created_on as created_at, updated_on as updated_at FROM `day_offs`";
        $results = DB::connection('mysql_active_collab')->select($sql, []);


        foreach (json_decode(json_encode($results), true) as $result) {
            $dayOff = DayOff::create($result);

            DB::insert('INSERT INTO `country_day_offs` (country_id, day_off_id) VALUES (?, ?)', [
                1,
                $dayOff->id
            ]);
        }

    }
}

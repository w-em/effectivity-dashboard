<?php

namespace Database\Seeders;

use App\Models\ArticleAttribute;
use App\Models\Country;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AvailabilitySeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(DayOffSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProjectSeeder::class);
        $this->call(ProjectTypeSeeder::class);
        $this->call(ProjectCategorySeeder::class);
        $this->call(TimeRecordTypesSeeder::class);
        $this->call(TimeRecordsSeeder::class);
        $this->call(PersonalSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(TasksSeeder::class);
    }
}

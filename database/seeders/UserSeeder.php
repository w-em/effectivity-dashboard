<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'sj@w-em.com',
            'email' => 'sj@w-em.com',
            'password' => bcrypt('sommer83')
        ]);

        \App\Models\User::create([
            'name' => 'nwo@w-em.com',
            'email' => 'nwo@w-em.com',
            'password' => bcrypt('nwo@w-em.com')
        ]);

    }
}

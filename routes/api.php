<?php

// use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
// use App\Http\Controllers\Auth\RegisterController;
// use App\Http\Controllers\Auth\ResetPasswordController;
// use App\Http\Controllers\Auth\VerificationController;

use App\Http\Controllers\ProjectController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\Report\PersonalReportController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\CountryController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LoginController::class, 'logout']);
    Route::post('refresh', [LoginController::class, 'refresh']);
    Route::get('me', [LoginController::class, 'me']);
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'projects'
], function () {
    Route::get('/', [ProjectController::class, 'index']);
    Route::get('/reports', [ProjectController::class, 'reports']);
    Route::get('/{id}', [ProjectController::class, 'show'])->where('id', '[0-9]+');
    Route::put('/{id}', [ProjectController::class, 'update'])->where('id', '[0-9]+');
    Route::get('/{id}/reportOverview', [ProjectController::class, 'reportOverview'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'teams'
], function () {
    Route::get('/', [TeamController::class, 'index']);
    Route::get('/{id}', [TeamController::class, 'show'])->where('id', '[0-9]+');
    Route::get('/{id}/reportOverview', [TeamController::class, 'reportOverview'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'personals'
], function () {
    Route::get('/', [PersonalController::class, 'index']);
    Route::get('/reports', [PersonalController::class, 'reports']);
    Route::get('/{id}', [PersonalController::class, 'show'])->where('id', '[0-9]+');

    Route::get('/{id}/reportOverview', [PersonalController::class, 'reportOverview'])->where('id', '[0-9]+');
    Route::get('/{id}/projectDetails', [PersonalController::class, 'projectDetails'])->where('id', '[0-9]+');
    Route::get('/{id}/projects', [PersonalController::class, 'projects'])->where('id', '[0-9]+');
    Route::get('/{id}/holidays/{year}', [PersonalController::class, 'holidays'])->where('id', '[0-9]+')->where('year', '[0-9]+');
    Route::put('/{id}', [PersonalController::class, 'update'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'companies'
], function () {
    Route::get('/', [CompanyController::class, 'index']);
    Route::get('/{id}', [CompanyController::class, 'show'])->where('id', '[0-9]+');
    Route::get('/{id}/reportOverview', [CompanyController::class, 'reportOverview'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'report/personals'
], function () {
    Route::get('/', [PersonalReportController::class, 'index']);
    Route::get('/{id}', [PersonalController::class, 'show'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api'],
    'prefix' => 'countries'
], function () {
    Route::get('/', [CountryController::class, 'index']);
    Route::get('/{id}', [CountryController::class, 'show'])->where('id', '[0-9]+');
    Route::put('/{id}', [CountryController::class, 'update'])->where('id', '[0-9]+');
});

Route::group([
    'middleware' => ['api']
], function () {
    Route::get('/{all}',function () {
        $d = [
            'message' => 'not found'
        ];

        return response( $d, 404, []);
    })->where('all','(.*)');
});

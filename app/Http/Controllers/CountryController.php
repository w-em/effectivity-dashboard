<?php

namespace App\Http\Controllers;

use App\Models\Country;

class CountryController extends AbstractModelController
{
    /**
     * like \App\Models\Country
     * @var string $modelName
     */
    protected $modelName = Country::class;

}

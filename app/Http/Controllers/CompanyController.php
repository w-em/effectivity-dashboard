<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Report\CompanyReport;
use App\Models\Report\PersonalReport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CompanyController extends AbstractModelController
{
    /**
     * like \App\Models\Company
     * @var string $modelName
     */
    protected $modelName = Company::class;

    protected function appendIndexQuery($query) {
        $query = $query->whereNull('archived_at');
        return $query;
    }

    public function reportOverview(Request $request, $id) {
        $dateFrom = $request->input('from', (new Carbon())->setMonth(1)->setDay(1)->format('Y-m-d'));
        $dateTo = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);

        $report = CompanyReport::findReportById($id, $dateFrom, $dateTo);

        return $this->sendResponseOk([
            'effectivity' => $report->getEffectivity(),
            'estimates' => $report->getEstimates(),
            'tracked' => $report->getTracked(),
            'projects' => $report->getProjects(),
            'tasks' => $report->getTasks(),
            'types' => $report->getRecordTypes(),
        ]);
    }
}

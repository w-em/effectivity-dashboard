<?php

namespace App\Http\Controllers;

use App\Models\Availability;
use App\Models\Personal;
use App\Models\Report\PersonalReport;
use App\Services\PersonalService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PersonalController extends AbstractModelController
{
    /**
     * like \App\Models\Personal
     * @var string $modelName
     */
    protected $modelName = Personal::class;

    protected function appendIndexQuery($query) {
        $query = $query->whereNull('archived_at');
        return $query;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function reports(Request $request)
    {
        $limit = (int)$request->get('itemsPerPage', 50);
        $page = (int)$request->get('page', 1);
        $sortBy = $request->get('sortBy', []);
        $sortDesc = $request->get('sortDesc', []);
        $term = trim($request->get('term', ''));
        $from = $request->input('from', (new Carbon())->setDay(1)->setMonth(1)->format('Y-m-d'));
        $to = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($from);
        $dateTo = Carbon::parse($to);

        $query = new $this->modelName;
        $model = new $this->modelName;
        $modelFillables = $model->getFillable();

        if (!empty($term)) {
            $query = $this->modelName::when($term, function ($query) use ($term, $modelFillables) {
                foreach ($modelFillables as $field) {
                    $query->orWhere($field,'like','%'.$term.'%');
                }

                return $query;
            });
        }

        foreach ($sortBy as $key => $value) {

            $orderDir = 'asc';
            if (key_exists($key, $sortDesc)) {
                $orderDir = strtolower($sortDesc[$key]) === "true" ? 'desc' : 'asc';
            }

            if (in_array($value, $modelFillables)) {
                $query = $query->orderBy($value, $orderDir);
            }
        }
        $query = $query->whereNull('archived_at');
        $result = $query->get()->makeHidden('availabilities');

        $paginator = self::paginate($result, $limit, $page);
        $items = $paginator->items();

        $periods = CarbonPeriod::create($dateFrom, '1 month', $dateTo);

        foreach ($items as $key => $item) {

            $monthly = [];
            foreach ($periods as $periodKey => $period) {
                $startOfMonth = $period->copy()->startOfMonth();
                $endOfMonth = $period->copy()->endOfMonth();

                if($endOfMonth->gt(now())) {
                    $endOfMonth = now();
                }

                $report = PersonalReport::findReportById($item->id, $startOfMonth, $endOfMonth);

                $monthly[] = [
                    'date' => $period->format('m-y'),
                    'hoursSoFar' => $report->getHoursSoFar(),
                    'hoursTracked' => $report->getTracked(),
                    'effectivity' => $report->getEffectivity()
                ];
            }

            $item->report = $monthly;


            $items[$key] = $item;
        }

        return $this->sendResponseOk([
            'items' => array_values($items),
            'total' => $paginator->total(),
            'limit' => $paginator->perPage()
        ]);
    }

    /**
     * @param Model $item
     * @return Model
     */
    public function beforeShow(Model $item) {
        return $item;
    }

    public function reportOverview(Request $request, $id) {

        $dateFrom = $request->input('from', (new Carbon())->setMonth(1)->setDay(1)->format('Y-m-d'));
        $dateTo = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);

        $report = PersonalReport::findReportById($id, $dateFrom, $dateTo);

        $result = [
            'hours_daily' => $report->getHoursDaily(),
            'hours_so_far' => $report->getHoursSoFar(),
            'tracked' => $report->getTracked(),
            'month_timereports' => $report->getMonthlyTimeRecordTotals(),
            'tracked_by_projects' => $report->getProjects(),
            'vacation_days' => $report->getUnavailableTotalDays(),
            'vacation_days_hours' => $report->getUnavailableTotalDays() * $report->getHoursDaily(),
            'day_offs' => $report->getDayOffTotalDays(),
            'day_off_hours' => $report->getDayOffTotalDays() * $report->getHoursDaily(),
            'effectivity' => $report->getEffectivity(),
        ];

        return $result;
    }

    public function holidays(Request $request, $id, $year) {
        $results = Availability::whereYear('start_date', '=', $year)
            ->where([
                ['personal_id', '=', $id],
                ['availability_type_id', '=', 2]
            ])->orderby('start_date', 'desc')->get();

        return $this->sendResponseOk([
            "items" => $results
        ]);
    }

    public function projectDetails(Request $request, $id) {
        $dateFrom = $request->input('from', (new Carbon())->setDay(1)->format('Y-m-d'));
        $dateTo = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);

        $report = PersonalReport::findReportById($id, $dateFrom, $dateTo);

        return $this->sendResponseOk(array_values($report->getProjectDetails()->toArray()));
    }

    public function projects(Request $request, $id) {
        $dateFrom = $request->input('from', (new Carbon())->setDay(1)->format('Y-m-d'));
        $dateTo = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);

        $report = PersonalReport::findReportById($id, $dateFrom, $dateTo);

        return $this->sendResponseOk($report->getProjects());
    }

}

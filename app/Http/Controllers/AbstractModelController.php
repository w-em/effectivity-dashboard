<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class AbstractModelController extends Controller implements ModelControllerInterface
{
    /**
     * like \App\Models\Article
     * @var string $modelName
     */
    protected $modelName = '';

    /**
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = (int)$request->get('itemsPerPage', 50);
        $page = (int)$request->get('page', 1);
        $sortBy = $request->get('sortBy', []);
        $sortDesc = $request->get('sortDesc', []);
        $term = trim($request->get('term', ''));

        $query = new $this->modelName;
        $model = new $this->modelName;
        $modelFillables = $model->getFillable();

        if (!empty($term)) {
            $query = $this->modelName::when($term, function ($query) use ($term, $modelFillables) {
                foreach ($modelFillables as $field) {
                    $query->orWhere($field,'like','%'.$term.'%');
                }

                return $query;
            });
        }

        foreach ($sortBy as $key => $value) {

            $orderDir = 'asc';
            if (key_exists($key, $sortDesc)) {
                $orderDir = strtolower($sortDesc[$key]) === "true" ? 'desc' : 'asc';
            }

            if (in_array($value, $modelFillables)) {
                $query = $query->orderBy($value, $orderDir);
            }
        }

        if(method_exists($this, 'appendIndexQuery')) {
            $query = $this->appendIndexQuery($query);
        }

        $result = $this->beforeIndexPagination($query->get());

        $paginator = self::paginate($result, $limit, $page);
        $items = $paginator->items();

        if(method_exists($this, 'afterIndexPagination')) {
            $items = $this->afterIndexPagination($items);
        }

        return $this->sendResponseOk([
            'items' => array_values($items),
            'total' => $paginator->total(),
            'limit' => $paginator->perPage()
        ]);
    }

    /**
     * @param Collection $items
     * @return Collection
     */
    public function beforeIndexPagination(Collection $items) {
        return $items;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id) {

        $item = $this->modelName::findOrFail($id);

        $item = $this->beforeShow($item);

        return $this->sendResponseOk($item);
    }

    /**
     * @param Model $item
     * @return Model
     */
    public function beforeShow(Model $item) {
        return $item;
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id) {
        try {
            $item = $this->modelName::findOrFail($id);
            $params = $request->all();
            $item->update($params);

            return $this->sendResponseUpdated($item);
        } catch (ModelNotFoundException $exception) {
            return $this->sendResponseNotFound();
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, int $id) {}

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        try {
            $params = $request->all();
            $item = $this->modelName::create($params);

            return $this->sendResponseUpdated($item);
        } catch (ModelNotFoundException $exception) {
            return $this->sendResponseNotFound();
        }
    }

    /**
     * creates a pagination of collection of items
     *
     * @param array|Collection $items
     * @param int $perPage
     * @param int $page
     * @param array $options
     *
     * @return LengthAwarePaginator
     */
    public static function paginate($items, $perPage = 12, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}

<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\AbstractModelController;
use App\Models\Personal;
use App\Models\Report\PersonalReport;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class PersonalReportController extends AbstractModelController
{
    /**
     * like \App\Models\Member
     * @var string $modelName
     */
    protected $modelName = Personal::class;

    protected function appendIndexQuery($query) {
        $query = $query->whereNull('archived_at');
        return $query;
    }

    protected function afterIndexPagination($personals) {
        $dateFrom = now()->format('Y-01-01');
        $dateTo = now()->format('Y-12-31');

        foreach ($personals as $personalKey => $personal) {

            $periods = CarbonPeriod::create($dateFrom, '1 month', $dateTo);

            $report = [];

            foreach ($periods as $periodKey => $period) {
                $personalReport = new PersonalReport();
                $startOfMonth = $period->copy()->startOfMonth();
                $endOfMonth = $period->copy()->endOfMonth();

                if($endOfMonth->gt(now())) {
                    $endOfMonth = now();
                }

                $personalReport->setPersonalId($personal->id);
                $personalReport->setDateFrom($startOfMonth);
                $personalReport->setDateTo($endOfMonth);

                $hoursSoFar = $personalReport->getHoursSoFar();
                $hoursTracked = $personalReport->getHoursTracked();
                if ($hoursSoFar > 0) {
                    $effectivity = round($hoursTracked['billable'] / $hoursSoFar * 100, 2);
                }else {
                    $effectivity = 0;
                }
                $report[] = [
                    'date' => $period->format('m-y'),
                    'hoursSoFar' => $hoursSoFar,
                    'hoursTracked' => $hoursTracked,
                    'effectivity' => $effectivity
                ];
            }


            $personal->report = $report;

            $personals[$personalKey] = $personal;
        }

        return $personals;
    }


}

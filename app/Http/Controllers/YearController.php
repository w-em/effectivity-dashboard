<?php

namespace App\Http\Controllers;

use App\Models\Year;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class YearController extends AbstractModelController
{
    /**
     * like \App\Models\Article
     * @var string $modelName
     */
    protected $modelName = Year::class;
}

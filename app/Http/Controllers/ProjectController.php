<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Report\PersonalReport;
use App\Models\Report\ProjectReport;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class ProjectController extends AbstractModelController
{
    /**
     * like \App\Models\Article
     * @var string $modelName
     */
    protected $modelName = Project::class;

    protected function appendIndexQuery($query) {
        $query = $query->whereNull('completed_at');
        return $query;
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function reports(Request $request)
    {
        $limit = (int)$request->get('itemsPerPage', 50);
        $page = (int)$request->get('page', 1);
        $sortBy = $request->get('sortBy', []);
        $sortDesc = $request->get('sortDesc', []);
        $term = trim($request->get('term', ''));
        $from = $request->input('from', (new Carbon())->setDay(1)->setMonth(1)->format('Y-m-d'));
        $to = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($from);
        $dateTo = Carbon::parse($to);

        $query = new $this->modelName;
        $model = new $this->modelName;
        $modelFillables = $model->getFillable();

        if (!empty($term)) {
            $query = $this->modelName::when($term, function ($query) use ($term, $modelFillables) {
                foreach ($modelFillables as $field) {
                    $query->orWhere($field,'like','%'.$term.'%');
                }

                return $query;
            });
        }

        foreach ($sortBy as $key => $value) {

            $orderDir = 'asc';
            if (key_exists($key, $sortDesc)) {
                $orderDir = strtolower($sortDesc[$key]) === "true" ? 'desc' : 'asc';
            }

            if (in_array($value, $modelFillables)) {
                $query = $query->orderBy($value, $orderDir);
            }
        }
        $result = $query->get();

        $paginator = self::paginate($result, $limit, $page);
        $items = $paginator->items();

        $periods = CarbonPeriod::create($dateFrom, '1 month', $dateTo);

        foreach ($items as $key => $item) {

            $monthly = [];
            foreach ($periods as $periodKey => $period) {
                $startOfMonth = $period->copy()->startOfMonth();
                $endOfMonth = $period->copy()->endOfMonth();

                if($endOfMonth->gt(now())) {
                    $endOfMonth = now();
                }

                $report = ProjectReport::findReportById($item->id, $startOfMonth, $endOfMonth);

                $monthly[] = [
                    'date' => $period->format('m-y'),
                    'hoursSoFar' => $report->getTotal(),
                    'hoursTracked' => $report->getTracked(),
                    'effectivity' => $report->getEffectivity()
                ];
            }

            $item->report = $monthly;

            $items[$key] = $item;
        }

        return $this->sendResponseOk([
            'items' => array_values($items),
            'total' => $paginator->total(),
            'limit' => $paginator->perPage()
        ]);
    }

    public function reportOverview(Request $request, $id) {

        $dateFrom = $request->input('from', (new Carbon())->setMonth(1)->setDay(1)->format('Y-m-d'));
        $dateTo = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);

        $report = ProjectReport::findReportById($id, $dateFrom, $dateTo);

        $result = [
            'estimate' => $report->getEstimates(),
            'effectivity' => $report->getEffectivity(),
            'hours_tracked' => $report->getTotal(),
            'tracked_by_tickets' => $report->getTasks(),
            'tracked_by_type' => $report->getRecordTypes(),
        ];
        return $result;
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Report\CompanyReport;
use App\Models\Report\PersonalReport;
use App\Models\Report\TeamReport;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TeamController extends AbstractModelController
{
    /**
     * like \App\Models\Team
     * @var string $modelName
     */
    protected $modelName = Team::class;

    public function reportOverview(Request $request, $id) {
        $dateFrom = $request->input('from', (new Carbon())->setMonth(1)->setDay(1)->format('Y-m-d'));
        $dateTo = $request->input('to', now()->format('Y-m-d'));

        $dateFrom = Carbon::parse($dateFrom);
        $dateTo = Carbon::parse($dateTo);

        $report = TeamReport::findReportById($id, $dateFrom, $dateTo);

        $result = [
            'estimate' => $report->getEstimates(),
            'effectivity' => $report->getEffectivity(),
            'tracked' => $report->getTracked(),
            'projects' => $report->getProjects(),
            'companies' => $report->getCompanies(),
            // 'tracked_by_tickets' => $report->getTasks(),
            'types' => $report->getRecordTypes(),
        ];

        return $this->sendResponseOk($result);
    }
}

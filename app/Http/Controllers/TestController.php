<?php

namespace App\Http\Controllers;

use App\Jobs\ImportUpdatedTimeRecordsJob;

use App\Models\Report\PersonalReport;
use App\Models\Report\ProjectReport;
use Carbon\Carbon;


class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $report = ProjectReport::findReportById(1, (new Carbon())->setMonth(1)->setDay(1), now());
        // dump($report->toArray());
        dd($report->getEffectivity());
        // $personal = Personal::findOrFail(9);
        // $result = PersonalService::getPersonalWorkingDaysInRange($personal, now()->setMonth(1)->setDay(1), now());
        /*
        $personalReport = new PersonalReport();
        $personalReport->setDateFrom((new Carbon())->setMonth(1)->setDay(1));
        $personalReport->setDateTo(now());
        $personalReport->setPersonalIds([9]);

        $result = [
            'hours_so_far' => $personalReport->getHoursSoFar(),
            'hours_tracked' => $personalReport->getHoursTracked(),
            'month_timereports' => $personalReport->getMonthlyTimeRecordTotals(),
            'tracked_by_projects' => $personalReport->getProjectTracked(),
        ];
        */
        // dd($result);
    }

    public function importAll() {
        ImportUpdatedTimeRecordsJob::dispatchSync();
}

    public function loadTimeRecords() {
        $sql = "select * from time_records where updated_on BETWEEN '2021-09-30' and '2021-10-02'";
        $timeRecords = DB::connection('mysql_active_collab')->select($sql, []);
        dd($timeRecords);
    }
}

<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\Personal;
use App\Models\Project;
use App\Models\TimeRecord;
use App\Services\ImportService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportPersonalJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * the offset of current request
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $limit = 1000;

    protected $date = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($offset = 0, $limit = 100, $date = null)
    {
        if (is_null($date)) {
            $this->date = now();
        } else {
            $this->date = Carbon::parse($date);
        }
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $results = $this->getUpdated();

        foreach ($results as $result) {
            $id = (int) $result->id;
            $data = (array) $result;
            unset($data["id"]);
            unset($data["weekly_work_hours"]);

            Personal::updateOrCreate(
                [
                    "id" => $id
                ],
                $data
            );
        }


    }

    protected function getUpdated()
    {
        $sql = "SELECT id, CONCAT(first_name, ' ', last_name) as name, first_name, last_name, email, 40 as weekly_work_hours, archived_on as archived_at FROM `users` where type != 'Client' and  updated_on >= ?";
        $results = DB::connection('mysql_active_collab')->select($sql, [$this->date->format('Y-m-d')]);
        return $results;

    }
}

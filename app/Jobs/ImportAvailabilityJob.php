<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\Availability;
use App\Models\Personal;
use App\Models\Project;
use App\Models\TimeRecord;
use App\Services\ImportService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportAvailabilityJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * the offset of current request
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $limit = 1000;

    protected $date = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($offset = 0, $limit = 100, $date = null)
    {
        if (is_null($date)) {
            $this->date = now();
        } else {
            $this->date = Carbon::parse($date);
        }
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $results = $this->getUpdated();

        foreach ($results as $result) {
            $id = (int) $result->id;
            $data = (array) $result;
            unset($data["id"]);
            unset($data["weekly_work_hours"]);

            Availability::updateOrCreate(
                [
                    "id" => $id
                ],
                $data
            );
        }
    }



    protected function getUpdated()
    {
        $sql = "SELECT  id, message as name, start_date, end_date, availability_type_id, created_on as created_at, updated_on as updated_at, user_id as personal_id FROM `availability_records`";
        $records = DB::connection('mysql_active_collab')->select($sql, []);

        return $records;

    }
}

<?php

namespace App\Jobs;

use App\Models\Article;
use App\Models\TimeRecord;
use App\Services\ImportService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ImportUpdatedTimeRecordsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 25;

    /**
     * The maximum number of exceptions to allow before failing.
     *
     * @var int
     */
    public $maxExceptions = 3;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 60;

    /**
     * the offset of current request
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $limit = 1000;

    protected $date = null;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($offset = 0, $limit = 100, $date = null)
    {
        if (is_null($date)) {
            $this->date = now();
        } else {
            $this->date = Carbon::parse($date)->subDays(7);
        }
        $this->offset = $offset;
        $this->limit = $limit;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $results = $this->getUpdatedTimeRecordsList();

        foreach ($results as $result) {
            $id = (int) $result->id;
            $data = (array) $result;
            unset($data["id"]);

            TimeRecord::updateOrCreate(
                [
                    "id" => $id
                ],
                $data
            );
        }


    }

    protected function getUpdatedTimeRecordsList()
    {
        $sql = "SELECT id, summary as name, user_id as personal_id, billable_status as is_billable, job_type_id as record_type_id, parent_id as task_id, value, record_date, created_on as created_at, updated_on as updated_at, trashed_on as deleted_at
 FROM `time_records` WHERE parent_type = 'TASK' and user_id is not null and updated_on >= ?";
        $results = DB::connection('mysql_active_collab')->select($sql, [$this->date->format('Y-m-d')]);
        return $results;

    }
}

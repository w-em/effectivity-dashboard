<?php

namespace App\Console\Commands;

use App\Jobs\ImportAvailabilityJob;
use App\Jobs\ImportUpdatedTimeRecordsJob;
use Illuminate\Console\Command;

class ImportPersonalCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'dashboard:import:personal';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'import personal';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      ImportUpdatedTimeRecordsJob::dispatchSync();

  }
}

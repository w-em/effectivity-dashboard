<?php

namespace App\Console\Commands;

use App\Jobs\ImportAvailabilityJob;
use Illuminate\Console\Command;

class ImportAvailabilitiesCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'dashboard:import:availabilities';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'import availabilities';

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      ImportAvailabilityJob::dispatchSync();

  }
}

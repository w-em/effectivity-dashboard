<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleAttribute;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\Category;
use App\Models\Personal;
use App\Models\Year;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TeamTNT\TNTSearch\Indexer\TNTIndexer;
use TeamTNT\TNTSearch\TNTSearch;

class PersonalService {

    public static function getPersonalWorkingDaysInRange(Personal $personal, \DateTime $dateFrom, \DateTime $dateTo) {
        $startDate = Carbon::parse($dateFrom)->setHour(0)->setMinutes(0)->setSecond(0)->setMillisecond(0);
        $endDate = Carbon::parse($dateTo)->setHour(0)->setMinutes(0)->setSecond(0)->setMillisecond(1);

        $dayOffsAndHolidays = [];
        foreach($personal->availabilities as $availability) {

            $period = CarbonPeriod::create($availability->start_date->format('Y-m-d'), $availability->end_date->format('Y-m-d'));
            // Iterate over the period

            foreach ($period as $date) {
                $dayOffsAndHolidays[] = $date;
            }
        }

        $days = $startDate->diffInDaysFiltered(function (Carbon $date) use ($dayOffsAndHolidays) {
            return $date->isWeekday() && !in_array($date, $dayOffsAndHolidays);
        }, $endDate);

        return $days;
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Personal extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'first_name',
        'last_name',
        'weekly_work_hours',
        'vacation',
        'created_at', 'deleted_at', 'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'archive', 'deleted_at', 'updated_at'
    ];

    protected $with = [];
    protected $appends = [
        'team',
        'short_name'
    ];

    public function teams() {
        return $this->belongsToMany(Team::class, 'team_personals', 'personal_id');
    }

    public function getShortNameAttribute() {
        if (is_null($this->first_name) && is_null($this->last_name)) {
            return $this->email;
        }
        return substr($this->first_name, 0, 1) . '. ' . $this->last_name;
    }

    /**
     * get the default TEAM
     * @return Team|null
     */
    public function getTeamAttribute() {
        $results = DB::select('SELECT * FROM team_personals WHERE personal_id = ? and is_default = 1', [$this->id]);

        $teamId = 0;
        if (count($results) > 0) {
            foreach ($results as $result) {
                $teamId = $result->team_id;
            }
        }

        if ($teamId) {
            return Team::findOrFail($teamId);
        }

        return null;
    }

    public function availabilities() {
        return $this->hasMany(Availability::class);
    }
}

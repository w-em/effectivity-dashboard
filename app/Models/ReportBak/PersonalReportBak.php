<?php

namespace App\Models\ReportBak;

use App\Models\DayOff;
use App\Models\Personal;
use App\Services\PersonalService;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class PersonalReportBak
{
    /**
     * @var Carbon::now();
     */
    protected $dateFrom = null;

    /**
     * @var Carbon::now();
     */
    protected $dateTo = null;

    /**
     * @var int
     */
    protected $personalId = 0;

    /**
     * @var Personal
     */
    protected $personal = null;

    protected $dayOffs = null;
    protected $unavailableDays = null;

    /**
     * @return float|int
     */
    public function getHoursSoFar() {

        $personal = $this->personal;
        if ($this->dateFrom < $personal->created_at) {
            $this->dateFrom = $personal->created_at;
        }

        $personalWorkingdays = PersonalService::getPersonalWorkingDaysInRange($personal, $this->dateFrom, $this->dateTo);
        $hoursPerDay = $personal->weekly_work_hours / 5;
        $hoursSoFar = round($personalWorkingdays * $hoursPerDay, 2);

        return $hoursSoFar;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getProjectDetails() {
        $sql = "SELECT
                    t.id AS task_id,
                    t.name AS task_name,
                    tr.is_billable,
                    p.id as project_id,
                    p.name as project_name,
                    sum(value) AS total
                FROM
                    time_records AS tr
                    LEFT JOIN tasks AS t ON t.id = tr.task_id
                    LEFT JOIN projects AS p ON p.id = t.project_id
                WHERE
                    tr.record_date BETWEEN ? and ?
                    AND tr.personal_id = ?
                GROUP BY tr.id, tr.personal_id";
        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d'),
            $this->personalId
        ];
        $timeRecords = DB::select($sql, $params);

        $projects = collect($this->getProjects())->keyBy('project_id');

        foreach ($timeRecords as $timeRecord) {
            $projectId = $timeRecord->project_id;
            unset($timeRecord->project_id);
            unset($timeRecord->project_name);
            $projects[$projectId]->records[] = $timeRecord;
        }

        return $projects;
    }

    public function getProjects() {

        $sql = "SELECT
                    p.id as project_id,
                    p.name as project_name,
                    tr.is_billable,
                    sum(value) AS total
                FROM
                    time_records AS tr
                    LEFT JOIN tasks AS t ON t.id = tr.task_id
                    LEFT JOIN projects AS p ON p.id = t.project_id
                WHERE
                    tr.record_date BETWEEN ? and ?
                    AND tr.personal_id = ?
                GROUP BY
                    p.id, tr.is_billable";
        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d'),
            $this->personalId
        ];
        $results = DB::select($sql, $params);

        $projects = [];

        foreach ($results as $result) {
            $projectId = $result->project_id;

            $isBillable = $result->is_billable;
            $total = $result->total;

            unset($result->total);
            unset($result->is_billable);

            if (!key_exists($projectId, $projects)) {
                $projects[$projectId] = $result;
                $projects[$projectId]->billable = 0;
                $projects[$projectId]->not_billable = 0;
                $projects[$projectId]->total = 0;
            }

            $projects[$projectId]->billable = round($projects[$projectId]->billable + ($isBillable ? $total : 0), 2);
            $projects[$projectId]->not_billable = round($projects[$projectId]->not_billable + ($isBillable ? 0 : $total), 2);
            $projects[$projectId]->total = round($projects[$projectId]->total + $total, 2);
        }

        return array_values($projects);
    }

    /**
     * @return int[]
     */
    public function getHoursTracked() {

        $sql = "select
                tr.personal_id, tr.is_billable, sum(value) as total
                from time_records as tr
                left join tasks as t ON t.id = tr.task_id
                where tr.record_date BETWEEN ? and ?
                and
                tr.personal_id = ?
                    group by tr.personal_id, tr.is_billable";

        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d'),
            $this->personalId
        ];
        $results = DB::select($sql, $params);

        $total = 0;
        $billable = 0;
        $not_billable = 0;

        foreach ($results as $result) {
            if ($result->is_billable) {
                $billable = round($billable + $result->total, 2);
            } else {
                $not_billable = round($not_billable + $result->total, 2);
            }

            $total = round($total + $result->total, 2);
        }

        return [
            'total' => $total,
            'billable' => $billable,
            'not_billable' => $not_billable,
        ];
    }

    /**
     * @return array
     */
    public function getMonthlyTimeRecordTotals () {

        $sql = "select
                month(tr.created_at) as month, tr.personal_id, sum(tr.value) as total
                from time_records as tr
                where tr.record_date BETWEEN ? and ?
                and
                tr.personal_id = ?
                group by tr.personal_id, month(tr.created_at)";

        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d'),
            $this->personalId
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getUnavailablesDates() {
        if (is_null($this->unavailableDays)) {
            foreach($this->personal->availabilities as $availability) {
                $period = CarbonPeriod::create($availability->start_date->format('Y-m-d'), $availability->end_date->format('Y-m-d'));

                // Iterate over the period
                foreach ($period as $date) {
                    $holidays[] = $date;
                }
            }

            $this->unavailableDays = $holidays;
        }

        return $this->unavailableDays;
    }

    /**
     * @return float
     */
    public function getUnavailableTotalDays() {
        $startDate = $this->dateFrom;
        $endDate = $this->dateTo;

        $dayOffs = $this->getDayOffDates();
        $holidays = $this->getUnavailablesDates();

        $days = $startDate->diffInDaysFiltered(function (Carbon $date) use ($holidays, $dayOffs) {
            return !$date->isWeekend() && !in_array($date, $dayOffs) && in_array($date, $holidays);
        }, $endDate);


        return $days;
    }

    public function getHoursDaily() {
        return $this->personal->weekly_work_hours / 5;
    }

    public function getDayOffDates () {
        if (is_null($this->dayOffs)) {
            foreach(DayOff::all() as $dayOff) {

                if ($dayOff->repeat_yearly && $dayOff->start_date->year < $this->dateFrom->year) {
                    for($i = $dayOff->start_date->year; $i <= $this->dateFrom->year; $i++) {
                        $period = CarbonPeriod::create($dayOff->start_date->setYear($i)->format('Y-m-d'), $dayOff->end_date->setYear($i)->format('Y-m-d'));
                        // Iterate over the period
                        foreach ($period as $date) {
                            $dayOffs[] = $date;
                        }
                    }
                } else {
                    $period = CarbonPeriod::create($dayOff->start_date->format('Y-m-d'), $dayOff->end_date->format('Y-m-d'));
                    // Iterate over the period
                    foreach ($period as $date) {
                        $dayOffs[] = $date;
                    }
                }
            }

            $this->dayOffs = $dayOffs;
        }

        return $this->dayOffs;
    }

    public function getDayOffTotalDays() {
        $startDate = $this->dateFrom;
        $endDate = $this->dateTo;
        $dayOffs = $this->getDayOffDates();

        $days = $startDate->diffInDaysFiltered(function (Carbon $date) use ($dayOffs) {
            return !$date->isWeekend() && in_array($date, $dayOffs);
        }, $endDate);

        return $days;
    }

    /**
    public function getCapacity() {
        $hoursSoFar = $this->getHoursSoFar();
        $vacationDaysHours = $this->getVacationInDays() * $this->getHoursDaily();
        $dayOffHours = $this->getDayOffsInDays();
        $soll = $hoursSoFar + $vacationDaysHours + $dayOffHours;

        $result['hours_so_far'] = $hoursSoFar;
        $result['vacation_days_hours'] = $vacationDaysHours;
        $result['day_off_hours'] = $dayOffHours;
        $result['should_have'] = $soll;
        $hoursTracked = $this->getHoursTracked();
        $result['hours_tracked'] = $hoursTracked['billable'];
        if ($hoursSoFar > 0) {
            $result['effectivity'] = round($hoursTracked['billable'] / $hoursSoFar * 100, 2);
        }else {
            $result['effectivity'] = 0;
        }


        return $result;
    }
    */

    /**
     * @return Carbon
     */
    public function getDateFrom(): Carbon
    {
        return $this->dateFrom;
    }

    /**
     * @param Carbon $dateFrom
     */
    public function setDateFrom(CarbonInterface $dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return Carbon
     */
    public function getDateTo(): Carbon
    {
        return $this->dateTo;
    }

    /**
     * @param Carbon $dateTo
     */
    public function setDateTo(CarbonInterface $dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return int
     */
    public function getPersonalId(): int
    {
        return $this->personalId;
    }

    /**
     * @param int $personalId
     */
    public function setPersonalId(int $personalId)
    {
        $this->personalId = $personalId;
        $this->personal = Personal::findOrFail($personalId);
    }

}

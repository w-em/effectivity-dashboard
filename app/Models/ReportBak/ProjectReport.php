<?php

namespace App\Models\ReportBak;

use App\Models\DayOff;
use App\Models\Personal;
use App\Models\Project;
use App\Services\PersonalService;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class ProjectReport
{
    /**
     * @var Carbon::now();
     */
    protected $dateFrom = null;

    /**
     * @var Carbon::now();
     */
    protected $dateTo = null;

    /**
     * @var int
     */
    protected $projectId;

    /**
     * @var Project
     */
    protected $project;

    public function getEstimates () {
        $projectIds = $this->projectIds;
        $estimate = 0;

        foreach ($projectIds as $projectId) {
            $project = Project::findOrFail($projectId);

            switch ($project->type->id) {
                case 71: // monthly
                    $estimate = $estimate + round($this->dateFrom->floatDiffInMonths($this->dateTo) * $project->estimate);
                    break;
                case 83: // fixed_budget
                    $estimate = $estimate + round($estimate + $project->estimate);
                    break;
                case 84: // budget on demand
                case 85: // no budget
                    break;
            }
        }
        return $estimate;
    }

    public function getTicketsTrackedTime() {
        $idsQuestionMarks = substr(str_repeat('?,', count($this->projectIds)), 0, -1);
        $sql = "
        select t.id as task_id, tr.record_date, t.title as task_title, ROUND(sum(value), 2) as total
        from
            time_records as tr
        left join tasks as t ON t.id = tr.task_id
        left join projects as p on p.id = t.project_id
        where
            tr.record_date BETWEEN ? and ?
            AND
            p.id IN (" . $idsQuestionMarks . ")
        group by task_id
        ";

        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d')
        ];
        $params = array_merge($params, $this->projectIds);
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getTypeTrackedTime() {
        $idsQuestionMarks = substr(str_repeat('?,', count($this->projectIds)), 0, -1);
        $sql = "
        select
               trt.title as type_title, ROUND(sum(value), 2) as total
        from
            time_records as tr
        left join
                tasks as t ON t.id = tr.task_id
        left join
                time_record_types as trt on trt.id = tr.type_id
        left join projects as p on p.id = t.project_id
        where
            tr.record_date BETWEEN ? and ?
            AND
            p.id IN (" . $idsQuestionMarks . ")
        group by trt.id
        ";

        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d')
        ];
        $params = array_merge($params, $this->projectIds);
        $results = DB::select($sql, $params);

        return $results;
    }

    /**
     * @return int[]
     */
    public function getTrackedTime() {
        $personalIdsQuestionMarks = substr(str_repeat('?,', count($this->projectIds)), 0, -1);

        $sql = "select
                t.id as task_id, tr.record_date, t.title as task_title, tr.is_billable, sum(value) as total
                from
                     time_records as tr
                    left join tasks as t ON t.id = tr.task_id
                    left join projects as p on p.id = t.project_id
                where tr.record_date BETWEEN ? and ?
                and
                p.id in (" . $personalIdsQuestionMarks . ")
                    group by task_id, tr.is_billable";

        $params = [
            $this->dateFrom->format('Y-m-d'),
            $this->dateTo->format('Y-m-d')
        ];
        $params = array_merge($params, $this->projectIds);
        $results = DB::select($sql, $params);

        $total = 0;
        $billable = 0;
        $not_billable = 0;

        foreach ($results as $result) {
            if ($result->is_billable) {
                $billable = round($billable + $result->total, 2);
            } else {
                $not_billable = round($not_billable + $result->total, 2);
            }

            $total = round($total + $result->total, 2);
        }

        return [
            'total' => $total,
            'billable' => $billable,
            'not_billable' => $not_billable,
        ];
    }

    /**
     * @return Carbon
     */
    public function getDateFrom(): Carbon
    {
        return $this->dateFrom;
    }

    /**
     * @param Carbon $dateFrom
     */
    public function setDateFrom(Carbon $dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @return Carbon
     */
    public function getDateTo(): Carbon
    {
        return $this->dateTo;
    }

    /**
     * @param Carbon $dateTo
     */
    public function setDateTo(Carbon $dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return int
     */
    public function getProjectId(): array
    {
        return $this->projectId;
    }

    /**
     * @param int $projectId
     */
    public function setProjectId(array $projectId): void
    {
        $this->projectId = $projectId;
    }
}

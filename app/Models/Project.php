<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    const BUDGET_FIXED_IDS = [83];
    const BUDGET_MONTHLY_IDS = [71];
    const BUDGET_ON_DEMAND_IDS = [84];
    const BUDGET_INTERNAL_IDS = [85];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'estimate',
        'company_id',
        'category_id',
        'project_type_id'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'category_id', 'type_id', 'completed_at'
    ];

    protected $with = [
        'type',
        'category'
    ];

    public function category() {
        return $this->hasOne(ProjectCategory::class, 'id', 'category_id');
    }

    public function type() {
        return $this->hasOne(ProjectType::class, 'id', 'project_type_id');
    }

}

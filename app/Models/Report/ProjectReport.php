<?php

namespace App\Models\Report;

use App\Models\Project;
use Illuminate\Support\Facades\DB;

class ProjectReport extends AbstractReport
{
    protected $table = 'projects';

    protected $total = null;

    /**
     * @return int
     */
    public function getTotal() {

        $tracked = $this->getTracked();
        return $tracked['total'];
    }

    /**
     * @return int[]
     */
    public function getTracked()
    {

        if (!is_null($this->tracked)) {
            return $this->tracked;
        }

        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $sql = "select
                    prjtyp.id as project_type_id,
                    sum(`value`) as total
                from time_records AS rec
                LEFT JOIN tasks AS tsk ON tsk.id = rec.task_id
                LEFT JOIN projects AS prj ON prj.id = tsk.project_id
                LEFT JOIN project_types AS prjtyp ON prjtyp.id = prj.project_type_id
                where rec.record_date BETWEEN ? and ?
                and
                prj.id = ? AND prjtyp.id IS NOT NULL
                group by
                     prjtyp.id";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];

        $results = DB::select($sql, $params);

        $total = 0;
        $billable = 0;
        $not_billable = 0;

        foreach ($results as $result) {

            if (in_array($result->project_type_id, Project::BUDGET_INTERNAL_IDS)) {
                $not_billable = round($not_billable + $result->total, 2);
            } else {
                $billable = round($billable + $result->total, 2);
            }

            $total = round($total + $result->total, 2);
        }

        $tracked = [
            'total' => $total,
            'billable' => $billable,
            'not_billable' => $not_billable,
        ];

        $this->tracked = $tracked;
        return $this->tracked;

    }

    /**
     * @return array of tasks
     */
    public function getTasks() {
        $sql = "
            SELECT
                   tsk.id as task_id,
                   rec.record_date,
                   tsk.name as task_name,
                   ROUND(sum(value), 2) as total
            FROM time_records as rec
            LEFT JOIN tasks as tsk ON tsk.id = rec.task_id
            LEFT JOIN projects as prj ON prj.id = tsk.project_id
            LEFT JOIN project_types as prjtyp ON prjtyp.id = prj.project_type_id
            LEFT JOIN personals as pers ON pers.id = rec.personal_id
            where
                rec.record_date BETWEEN ? and ?
                AND
                prj.id = ?
            group by task_id
        ";

        $params = [
            $this->getDateFrom()->format('Y-m-d'),
            $this->getDateTo()->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    /**
     * @return array
     */
    public function getRecordTypes() {
        $sql = "
            SELECT
                   rec_type.id as type_id,
                   rec_type.name as type_name,
                   ROUND(sum(value), 2) as total
            FROM time_records as rec
            LEFT JOIN time_record_types as rec_type ON rec_type.id = rec.record_type_id
            LEFT JOIN tasks as tsk ON tsk.id = rec.task_id
            LEFT JOIN projects as prj ON prj.id = tsk.project_id
            LEFT JOIN project_types as prjtyp ON prjtyp.id = prj.project_type_id
            LEFT JOIN personals as pers ON pers.id = rec.personal_id
            where
                rec.record_date BETWEEN ? and ?
                AND
                prj.id = ?
            group by rec.record_type_id
        ";

        $params = [
            $this->getDateFrom()->format('Y-m-d'),
            $this->getDateTo()->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getEstimates() {

        if (in_array($this->project_type_id, Project::BUDGET_FIXED_IDS)) {
            return $this->estimate;
        }
        if (in_array($this->project_type_id, Project::BUDGET_ON_DEMAND_IDS)) {
            return $this->getTotal();
        }
        if (in_array($this->project_type_id, Project::BUDGET_MONTHLY_IDS)) {
            return round($this->getDateFrom()->floatDiffInMonths($this->getDateTo()) * $this->estimate);
        }
        if (in_array($this->project_type_id, Project::BUDGET_ON_DEMAND_IDS)) {
            return $this->getTotal();
        }
    }

    /*
    public function getEffectivity1() {
        return round($this->getEstimates() / $this->getTotal() * 100, 2);
    }
    */
    public function getEffectivity()
    {
        if (!is_null($this->effectivity)) {
            return $this->effectivity;
        }

        $hoursSoFar = $this->getEstimates();
        $tracked = $this->getTracked();

        $this->effectivity = [
            'total' => 0,
            'real' => 0
        ];

        if ($hoursSoFar > 0) {
            $this->effectivity = [
                'total' => $tracked['total'] > 0 ? round($hoursSoFar / $tracked['total'] * 100, 2) : 0,
                'real' => $tracked['billable'] > 0 ? round($hoursSoFar / $tracked['billable'] * 100, 2) : 0
            ];
        }

        return $this->effectivity;
    }
}

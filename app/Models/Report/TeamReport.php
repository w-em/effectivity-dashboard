<?php

namespace App\Models\Report;

use App\Models\Project;
use Illuminate\Support\Facades\DB;

class TeamReport extends AbstractReport
{
    protected $table = 'teams';

    protected $total = null;

    protected $projects = null;
    protected $tasks = null;
    protected $recordTypes = null;

    protected $projectReports = [];

    /**
     * @return int
     */
    public function getTotal() {

        $tracked = $this->getTracked();
        return $tracked['total'];
    }

    /**
     * @return int[]
     */
    public function getTracked()
    {
        if (!is_null($this->tracked)) {
            return $this->tracked;
        }

        $projects = $this->getProjects();
        $total = 0;
        $billable = 0;
        $not_billable = 0;

        foreach ($projects as $project) {
            if (!key_exists($project->project_id, $this->projectReports)) {
                $this->projectReports[$project->project_id] = ProjectReport::findReportById($project->project_id, $this->getDateFrom(), $this->getDateTo());
            }

            $projectReport = $this->projectReports[$project->project_id];

            $tracked = $projectReport->getTracked();
            $total = round($total + $tracked['total'], 2);
            $billable = round($billable + $tracked['billable'], 2);
            $not_billable = round($not_billable + $tracked['not_billable'], 2);
        }


        $tracked = [
            'total' => $total,
            'billable' => $billable,
            'not_billable' => $not_billable,
        ];

        $this->tracked = $tracked;
        return $this->tracked;

    }

    /**
     * @return array of tasks
     */
    public function getTasks() {

        if (!is_null($this->tasks)) {
            return $this->tasks;
        }

        $sql = "
            SELECT
                   tsk.id as task_id,
                   rec.record_date,
                   tsk.name as task_name,
                   ROUND(sum(value), 2) as total
            FROM time_records as rec
            LEFT JOIN team_personals as teamp ON teamp.personal_id = rec.personal_id
            LEFT JOIN tasks as tsk ON tsk.id = rec.task_id
            LEFT JOIN projects as prj ON prj.id = tsk.project_id
            LEFT JOIN companies as comp ON comp.id = prj.company_id
            LEFT JOIN project_types as prjtyp ON prjtyp.id = prj.project_type_id

            where
                rec.record_date BETWEEN ? and ?
                AND
                teamp.team_id = ?
            group by task_id
        ";

        $params = [
            $this->getDateFrom()->format('Y-m-d'),
            $this->getDateTo()->format('Y-m-d'),
            $this->id
        ];
        $this->tasks = DB::select($sql, $params);

        return $this->tasks;
    }

    public function getProjects()
    {
        if (!is_null($this->projects)) {
            return $this->projects;
        }

        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $sql = "SELECT
                    prj.id as project_id,
                    prj.name as project_name,
                    prjtyp.id as project_type_id,
                    prjtyp.name as project_type_name,
                    comp.id as company_id,
                    comp.name as company_name,
                    round(sum(value), 2) AS total
                FROM
                    time_records as rec
                    LEFT JOIN tasks as tsk ON tsk.id = rec.task_id
                    LEFT JOIN projects as prj ON prj.id = tsk.project_id
                    LEFT JOIN companies as comp ON comp.id = prj.company_id
                    LEFT JOIN project_types as prjtyp ON prjtyp.id = prj.project_type_id
                    LEFT JOIN team_personals as teamp ON teamp.personal_id = rec.personal_id
                WHERE
                    rec.record_date BETWEEN ? and ?
                    AND teamp.team_id = ?
                    AND prjtyp.id IS NOT NULL
                GROUP BY
                    prj.id";
        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $this->projects = DB::select($sql, $params);

        return $this->projects;
    }

    public function getCompanies () {
        return [];
    }

    /**
     * @return array
     */
    public function getRecordTypes() {

        if (!is_null($this->recordTypes)) {
            return $this->recordTypes;
        }

        $sql = "
            SELECT
                   rec_type.id as type_id,
                   rec_type.name as type_name,
                   ROUND(sum(value), 2) as total
            FROM time_records as rec
            LEFT JOIN time_record_types as rec_type ON rec_type.id = rec.record_type_id
            LEFT JOIN tasks as tsk ON tsk.id = rec.task_id
            LEFT JOIN projects as prj ON prj.id = tsk.project_id
            LEFT JOIN companies as comp ON comp.id = prj.company_id
            LEFT JOIN project_types as prjtyp ON prjtyp.id = prj.project_type_id
            LEFT JOIN team_personals as teamp ON teamp.personal_id = rec.personal_id
            where
                rec.record_date BETWEEN ? and ?
                AND
                teamp.team_id = ?
            group by rec.record_type_id
        ";

        $params = [
            $this->getDateFrom()->format('Y-m-d'),
            $this->getDateTo()->format('Y-m-d'),
            $this->id
        ];
        $this->recordTypes = DB::select($sql, $params);

        return $this->recordTypes;
    }

    public function getEstimates() {
        $projects = $this->getProjects();
        $total = 0;
        foreach ($projects as $project) {
            if (!key_exists($project->project_id, $this->projectReports)) {
                $this->projectReports[$project->project_id] = ProjectReport::findReportById($project->project_id, $this->getDateFrom(), $this->getDateTo());
            }

            $projectReport = $this->projectReports[$project->project_id];

            $estimates = $projectReport->getEstimates();

            $total = round($total + $estimates, 2);
        }
        return round($total, 2);
    }

    public function getEffectivity() {

        $projects = $this->getProjects();
        $total = 0;
        $real = 0;

        $this->effectivity = [
            'total' => 0,
            'real' => 0
        ];

        foreach ($projects as $project) {
            if (!key_exists($project->project_id, $this->projectReports)) {
                $this->projectReports[$project->project_id] = ProjectReport::findReportById($project->project_id, $this->getDateFrom(), $this->getDateTo());
            }
            $projectReport = $this->projectReports[$project->project_id];
            $effectivity = $projectReport->getEffectivity();
            $total = round($total + $effectivity['total'], 2);
            $real = round($real + $effectivity['real'], 2);
        }

        $this->effectivity = [
            'total' => $total,
            'real' => $real
        ];

        return $this->effectivity;
    }
}

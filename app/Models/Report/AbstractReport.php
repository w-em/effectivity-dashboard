<?php

namespace App\Models\Report;

use Carbon\CarbonInterface;
use http\Exception\UnexpectedValueException;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class AbstractReport extends Model
{
    /**
     * @var CarbonInterface
     */
    protected $dateFrom = null;

    /**
     * @var CarbonInterface
     */
    protected $dateTo = null;

    /**
     * @param $id
     * @param CarbonInterface $dateFrom
     * @param CarbonInterface $dateTo
     * @return PersonalReport
     */
    public static function findReportById($id, CarbonInterface $dateFrom, CarbonInterface $dateTo)
    {
        $result = self::find($id, ['*']);

        $id = $id instanceof Arrayable ? $id->toArray() : $id;

        if (is_array($id)) {
            if (count($result) === count(array_unique($id))) {
                return $result;
            }
        } elseif (!is_null($result)) {
            if ($dateFrom < $result->created_at) {
                $dateFrom = $result->created_at;
            }

            $result->setAttribute('dateFrom', $dateFrom);
            $result->setAttribute('dateTo', $dateTo);

            return $result;
        }

        throw (new ModelNotFoundException)->setModel(
            get_class(self::class), $id
        );
    }

    /**
     * @return CarbonInterface
     */
    public function getDateFrom(): ?CarbonInterface
    {
        return $this->getAttribute('dateFrom');
    }

    /**
     * @param CarbonInterface $dateFrom
     */
    public function setDateFrom(?CarbonInterface $dateFrom): void
    {
        $this->setAttribute('dateFrom', $dateFrom);
    }

    /**
     * @return CarbonInterface
     */
    public function getDateTo(): ?CarbonInterface
    {
        return $this->getAttribute('dateTo');
    }

    /**
     * @param CarbonInterface $dateTo
     */
    public function setDateTo(?CarbonInterface $dateTo): void
    {
        $this->setAttribute('dateTo', $dateTo);
    }
}

<?php

namespace App\Models\Report;

use App\Models\Availability;
use App\Models\DayOff;
use App\Models\Project;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class PersonalReport extends AbstractReport
{

    protected $table = 'personals';

    /**
     * @var array[CarbonInterface]
     */
    protected $availabilityDates = null;

    /**
     * @var array[Project]
     */
    protected $projects = null;

    protected $total = null;
    protected $tracked = null;
    protected $effectivity = null;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function availabilities() {
        return $this->hasMany(Availability::class, 'personal_id');
    }

    /*
     * get the availibily dates
     */
    public function getAvailabilitieDates() {

        if (is_null($this->availabilityDates)) {

            $this->availabilityDates = [];
            foreach ($this->availabilities as $availability) {

                $period = CarbonPeriod::create($availability->start_date->format('Y-m-d'), $availability->end_date->format('Y-m-d'));

                // Iterate over the period
                foreach ($period as $date) {
                    $this->availabilityDates[] = $date;
                }
            }
        }
        return $this->availabilityDates;
    }

    /**
     * get the working days
     * @return int
     */
    public function getWorkingDays ()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $startDate = $dateFrom->setHour(0)->setMinutes(0)->setSecond(0)->setMillisecond(0);
        $endDate = $dateTo->setHour(0)->setMinutes(0)->setSecond(0)->setMillisecond(1);

        $availabilityDates = $this->getAvailabilitieDates();

        $days = $startDate->diffInDaysFiltered(function (Carbon $date) use ($availabilityDates) {
            return $date->isWeekday() && !in_array($date, $availabilityDates);
        }, $endDate);

        return $days;
    }

    /**
     * @return float|int
     */
    public function getHoursSoFar()
    {
        $workingdays = $this->getWorkingDays();
        $hoursPerDay = $this->weekly_work_hours / 5;
        $hoursSoFar = round($workingdays * $hoursPerDay, 2);

        return $hoursSoFar;
    }

    /**
     * @return array
     */
    public function getEffectivity()
    {
        if (!is_null($this->effectivity)) {
            return $this->effectivity;
        }

        $hoursSoFar = $this->getHoursSoFar();
        $tracked = $this->getTracked();

        $this->effectivity = [
            'total' => 0,
            'real' => 0
        ];

        if ($hoursSoFar > 0) {
            $this->effectivity = [
                'total' => round($tracked['total'] / $hoursSoFar * 100, 2),
                'real' => round($tracked['billable'] / $hoursSoFar * 100, 2)
            ];
        }

        return $this->effectivity;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getProjectDetails()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();
        $sql = "SELECT
                    tr.id AS record_id,
                    tr.record_date,
                    tr.name as record_name,
                    t.id AS task_id,
                    t.name AS task_name,
                    p.id as project_id,
                    p.name as project_name,
                    sum(value) AS total
                FROM
                    time_records AS tr
                    LEFT JOIN tasks AS t ON t.id = tr.task_id
                    LEFT JOIN projects AS p ON p.id = t.project_id
                WHERE
                    tr.record_date BETWEEN ? and ?
                    AND tr.personal_id = ?
                GROUP BY tr.id";
        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $timeRecords = DB::select($sql, $params);

        $projects = collect($this->getProjects())->keyBy('project_id');

        foreach ($timeRecords as $timeRecord) {
            $projectId = $timeRecord->project_id;
            unset($timeRecord->project_id);
            unset($timeRecord->project_name);
            $projects[$projectId]->records[] = $timeRecord;
        }

        return $projects;
    }

    public function getProjects()
    {
        if (!is_null($this->projects)) {
            return $this->projects;
        }

        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $sql = "SELECT
                    prj.id as project_id,
                    prj.name as project_name,
                    prjtyp.id as project_type_id,
                    prjtyp.name as project_type_name,
                    sum(value) AS total
                FROM
                    time_records as rec
                    LEFT JOIN tasks as tsk ON tsk.id = rec.task_id
                    LEFT JOIN projects as prj ON prj.id = tsk.project_id
                    LEFT JOIN project_types as prjtyp ON prjtyp.id = prj.project_type_id
                WHERE
                    rec.record_date BETWEEN ? and ?
                    AND rec.personal_id = ?
                GROUP BY
                    prj.id";
        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $this->projects = DB::select($sql, $params);

        return $this->projects;
    }

    public function getTotal() {
        $tracked = $this->getTracked();
        return $tracked['total'];
    }

    /**
     * @return int[]
     */
    public function getTracked()
    {

        if (!is_null($this->tracked)) {
            return $this->tracked;
        }

        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $sql = "select prjtyp.id as project_type_id, sum(`value`) as total
                from time_records AS rec
                LEFT JOIN tasks AS tsk ON tsk.id = rec.task_id
                LEFT JOIN projects AS prj ON prj.id = tsk.project_id
                LEFT JOIN project_types AS prjtyp ON prjtyp.id = prj.project_type_id
                where rec.record_date BETWEEN ? and ?
                and
                rec.personal_id = ? AND prjtyp.id IS NOT NULL
                group by prjtyp.id";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];

        $results = DB::select($sql, $params);

        $total = 0;
        $billable = 0;
        $not_billable = 0;

        foreach ($results as $result) {

            if (in_array($result->project_type_id, Project::BUDGET_INTERNAL_IDS)) {
                $not_billable = round($not_billable + $result->total, 2);
            } else {
                $billable = round($billable + $result->total, 2);
            }

            $total = round($total + $result->total, 2);
        }

        $tracked = [
            'total' => $total,
            'billable' => $billable,
            'not_billable' => $not_billable,
        ];

        $this->tracked = $tracked;
        return $this->tracked;

    }

    /**
     * @return array
     */
    public function getMonthlyTimeRecordTotals()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $sql = "select
                month(tr.created_at) as month, tr.personal_id, sum(tr.value) as total
                from time_records as tr
                where tr.record_date BETWEEN ? and ?
                and
                tr.personal_id = ?
                group by tr.personal_id, month(tr.created_at)";

        $params = [
            $dateFrom,
            $dateTo,
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getUnavailablesDates()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();
        if (is_null($this->unavailableDays)) {
            foreach ($this->availabilities as $availability) {
                $period = CarbonPeriod::create($availability->start_date->format('Y-m-d'), $availability->end_date->format('Y-m-d'));

                // Iterate over the period
                foreach ($period as $date) {
                    $holidays[] = $date;
                }
            }

            $this->unavailableDays = $holidays;
        }

        return $this->unavailableDays;
    }

    /**
     * @return float
     */
    public function getUnavailableTotalDays()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();

        $dayOffs = $this->getDayOffDates($dateFrom, $dateTo);
        $holidays = $this->getUnavailablesDates($dateFrom, $dateTo);

        $days = $dateFrom->diffInDaysFiltered(function (CarbonInterface $date) use ($holidays, $dayOffs) {
            return !$date->isWeekend() && !in_array($date, $dayOffs) && in_array($date, $holidays);
        }, $dateTo);


        return $days;
    }

    public function getHoursDaily()
    {
        return $this->weekly_work_hours / 5;
    }

    public function getDayOffDates()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();
        if (is_null($this->dayOffs)) {

            foreach (DayOff::all() as $dayOff) {

                if ($dayOff->repeat_yearly && $dayOff->start_date->year < $dateFrom->year) {
                    for ($i = $dayOff->start_date->year; $i <= $dateFrom->year; $i++) {
                        $period = CarbonPeriod::create($dayOff->start_date->setYear($i)->format('Y-m-d'), $dayOff->end_date->setYear($i)->format('Y-m-d'));
                        // Iterate over the period
                        foreach ($period as $date) {
                            $dayOffs[] = $date;
                        }
                    }
                } else {
                    $period = CarbonPeriod::create($dayOff->start_date->format('Y-m-d'), $dayOff->end_date->format('Y-m-d'));
                    // Iterate over the period
                    foreach ($period as $date) {
                        $dayOffs[] = $date;
                    }
                }
            }

            $this->dayOffs = $dayOffs;
        }

        return $this->dayOffs;
    }

    public function getDayOffTotalDays()
    {
        $dateFrom = $this->getDateFrom();
        $dateTo = $this->getDateTo();
        $dayOffs = $this->getDayOffDates($dateFrom, $dateTo);

        $days = $dateFrom->diffInDaysFiltered(function (CarbonInterface $date) use ($dayOffs) {
            return !$date->isWeekend() && in_array($date, $dayOffs);
        }, $dateTo);

        return $days;
    }
}

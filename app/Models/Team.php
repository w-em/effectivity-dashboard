<?php

namespace App\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at', 'deleted_at'
    ];

    protected $with = [];

    public function personal() {
        return $this->belongsToMany(Personal::class, 'team_personals', 'team_id');
    }

    /**
     * @param CarbonInterface $dateFrom
     * @param CarbonInterface $dateTo
     * @return array
     */
    public function getTicketsTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "
        select t.id as task_id, tr.record_date, c.id as company_id, c.name as company_name, t.name as task_name, ROUND(sum(value), 2) as total
        from
            time_records as tr
        left join tasks as t ON t.id = tr.task_id
        left join personals as pers ON pers.id = tr.personal_id
        left join team_personals as ts ON ts.personal_id = pers.id and ts.is_default = 1
        left join teams as team ON team.id = ts.team_id
        left join projects as p on p.id = t.project_id
        left join companies as c on p.company_id = c.id

        where
            tr.record_date BETWEEN ? and ?
            AND
            team.id = ?
        group by task_id
        ";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getTypeTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "
            select
                   trt.name as type_name, c.id as company_id, c.name as company_name,  ROUND(sum(value), 2) as total
            from
                time_records as tr
            left join
                    tasks as t ON t.id = tr.task_id
            left join
                    time_record_types as trt on trt.id = tr.type_id
            left join
                    projects as p on p.id = t.project_id
            left join personals as pers ON pers.id = tr.personal_id
        left join team_personals as ts ON ts.personal_id = pers.id and ts.is_default = 1
        left join teams as team ON team.id = ts.team_id

            left join companies as c on p.company_id = c.id
            where
                tr.record_date BETWEEN ? and ?
                AND
                team.id = ?
            group by trt.id";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "select
                team.id, tr.is_billable, sum(value) as total
                from
                     time_records as tr
                    left join tasks as t ON t.id = tr.task_id
                    left join projects as p on p.id = t.project_id
                    left join companies as c on p.company_id = c.id
                    left join personals as pers ON pers.id = tr.personal_id
                    left join team_personals as ts ON ts.personal_id = pers.id and ts.is_default = 1
                    left join teams as team ON team.id = ts.team_id
                where tr.record_date BETWEEN ? and ?
                and
                team.id = ?
                    group by team.id, tr.is_billable";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getProjectsTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "
            select
                p.id as project_id, p.name as project_name, c.id as company_id, c.name as company_name, ROUND(sum(value), 2) as total
            from
                time_records as tr
            left join tasks as t ON t.id = tr.task_id
            left join projects as p on p.id = t.project_id
            left join companies as c on p.company_id = c.id
            left join personals as pers ON pers.id = tr.personal_id
            left join team_personals as ts ON ts.personal_id = pers.id and ts.is_default = 1
            left join teams as team ON team.id = ts.team_id
            where
                tr.record_date BETWEEN ? and ?
                AND
                team.id = ?
            group by p.id
        ";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }
}

<?php

namespace App\Models;

use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'address', 'homepage_url', 'phone', 'created_at', 'updated_at', 'archived_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'archived_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at'
    ];

    protected $with = [];

    /**
     * @param CarbonInterface $dateFrom
     * @param CarbonInterface $dateTo
     * @return array
     */
    public function getTicketsTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "
        select t.id as task_id, tr.record_date, c.id as company_id, c.name as company_name, t.name as task_name, ROUND(sum(value), 2) as total
        from
            time_records as tr
        left join tasks as t ON t.id = tr.task_id
        left join projects as p on p.id = t.project_id
        left join companies as c on p.company_id = c.id

        where
            tr.record_date BETWEEN ? and ?
            AND
            c.id = ?
        group by task_id
        ";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getTypeTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "
            select
                   trt.name as type_name, c.id as company_id, c.name as company_name,  ROUND(sum(value), 2) as total
            from
                time_records as tr
            left join
                    tasks as t ON t.id = tr.task_id
            left join
                    time_record_types as trt on trt.id = tr.type_id
            left join
                    projects as p on p.id = t.project_id
            left join companies as c on p.company_id = c.id
            where
                tr.record_date BETWEEN ? and ?
                AND
                c.id = ?
            group by trt.id";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "select
                t.id as task_id, tr.record_date, t.name as task_name, c.id as company_id, c.name as company_name, tr.is_billable, sum(value) as total
                from
                     time_records as tr
                    left join tasks as t ON t.id = tr.task_id
                    left join projects as p on p.id = t.project_id
                    left join companies as c on p.company_id = c.id
                where tr.record_date BETWEEN ? and ?
                and
                c.id = ?
                    group by task_id, tr.is_billable";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }

    public function getProjectsTrackedTime(CarbonInterface $dateFrom, CarbonInterface $dateTo) {
        $sql = "
            select
                p.id as project_id, p.name as project_name, c.id as company_id, c.name as company_name, ROUND(sum(value), 2) as total
            from
                time_records as tr
            left join tasks as t ON t.id = tr.task_id
            left join projects as p on p.id = t.project_id
            left join companies as c on p.company_id = c.id
            where
                tr.record_date BETWEEN ? and ?
                AND
                c.id = ?
            group by p.id
        ";

        $params = [
            $dateFrom->format('Y-m-d'),
            $dateTo->format('Y-m-d'),
            $this->id
        ];
        $results = DB::select($sql, $params);

        return $results;
    }
}
